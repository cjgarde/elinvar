import * as types from '../constants/AmountTypes';

export const updateAmount = amount => ({ type: types.UPDATE_AMOUNT, amount });
