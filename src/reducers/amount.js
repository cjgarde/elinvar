import { UPDATE_AMOUNT } from '../constants/AmountTypes';

const initialState = [
  {
    amount:0
  }
]

export default function amount(state = initialState, action) {
  switch (action.type) {
    case UPDATE_AMOUNT:
      return [
        {
          amount: action.amount
        },
        ...state
      ]
    default:
      return state
  }
}
