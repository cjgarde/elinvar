import React, { PropTypes, Component } from 'react';
import logo from './logo.svg';
import Widget from './components/widget';
import './App.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as AmountActions from './actions';

const App = ({amount, actions}) => (
  <div>
    <Widget value={amount[0]} updateAmount={actions.updateAmount} />
  </div>
)

App.propTypes = {
  amount: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  amount: state.amount
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(AmountActions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
