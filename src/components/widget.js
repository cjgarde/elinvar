import React, { Component, PropTypes } from 'react';
import 'rc-slider/assets/index.css';
import Rcslider from 'rc-slider';
import * as constants from '../constants/Slider';

export default class Widget extends React.Component {
  static propTypes = {
    value: PropTypes.object.isRequired,
    updateAmount: PropTypes.func.isRequired
  }

  _amountRender = () => {
    const amount = this.props.value.amount;
    const decimals = amount - Math.floor(amount);

    if (decimals > 0) {
      console.log(decimals)
      return decimals.toString().substr(decimals.toString());
    }
    else {
      return this.props.value.amount;
    }
  };

  _getDecimals = () => {
    const amount = this.props.value.amount.toString();
    const decimalsPosition = amount.indexOf('.');

    return decimalsPosition > -1 ? amount.substr(decimalsPosition + 1) : '';
  };

  _handleSave = amount => {
    if (amount) {
      this.props.updateAmount(amount);
    }
  };

  render() {
    const decimals = this._getDecimals();
    const floor = Math.floor(this.props.value.amount).toLocaleString('es-ES', {minimumFractionDigits: 0})

    return(
      <div>
        <span className="amount">
          { floor }
          <span className="decimals-separator"> { decimals !== '' ? ',' : ''}</span>
          <span className="decimals">{ decimals !== '' ? decimals : '' }</span>
          <span className="euro">€</span>
        </span>
        <Rcslider
          className="slider-elinvar"
          min={ constants.MIN }
          max={ constants.MAX }
          step={ constants.STEP }
          onChange={this._handleSave}
          />
      </div>
    )
  }
}
